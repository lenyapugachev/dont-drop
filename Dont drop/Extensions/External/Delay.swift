//
//  Delay.swift
//  Qtickets
//
//  Created by Aleksei Pugachev on 20/02/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

func delay(_ time: Double, _ block: @escaping (() -> ())) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + time) {
        block()
    }
}
