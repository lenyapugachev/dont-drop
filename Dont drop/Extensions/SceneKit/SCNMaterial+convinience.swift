//
//  SCNMaterial+convinience.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 19.08.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import SceneKit

class Material: SCNMaterial {
    
    init(diffuseColor: UIColor, specularColor: UIColor, illuminationColor: UIColor = .black) {
        super.init()
        
        lightingModel = .phong
        
        diffuse.contents = diffuseColor
        specular.contents = specularColor
        selfIllumination.contents = illuminationColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
