//
//  SCNVector3+Math.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 12/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

extension SCNVector3 {
    
    func angleBetweenVectors(_ vectorB: SCNVector3) -> CGFloat {
        return CGFloat(atan2(vectorB.x - x, vectorB.z - z) - .pi / 2)
    }
    
    func distanceBetweenVectors(_ vectorB: SCNVector3) -> CGFloat {
        return CGFloat(sqrt(pow(vectorB.x - x, 2) + pow(vectorB.z - z, 2)))
    }
    
}
