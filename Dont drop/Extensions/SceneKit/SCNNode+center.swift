//
//  SCNNode+center.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 17/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

extension SCNNode {
    
    func center() {
        let (minVec, maxVec) = boundingBox
        
        pivot = SCNMatrix4MakeTranslation((maxVec.x - minVec.x) / 2 + minVec.x, (maxVec.y - minVec.y) / 2 + minVec.y, 0)
    }
    
}
