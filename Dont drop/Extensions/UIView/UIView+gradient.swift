//
//  UIView+gradient.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 16/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

extension UIView {

    func gradient(colors: [CGColor]) {
        let layer: CAGradientLayer = CAGradientLayer()
        
        layer.frame.size = UIScreen.main.bounds.size
        layer.frame.origin = CGPoint.zero
        
        layer.startPoint = CGPoint.zero
        layer.endPoint = CGPoint(x: 0, y: 1)
        
        layer.colors = colors
        
        self.layer.insertSublayer(layer, at: 0)
    }

}
