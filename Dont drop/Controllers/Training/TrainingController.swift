//
//  TrainingController.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

class TrainingController {
    
    enum TrainingState {
        case prepare
        case going
        case over
    }
    
    weak var view: TrainingViewController?
    
    var state: TrainingState = .prepare
    
    var timer: Timer?
    
    var pveTaps = 0
    
    var tip = -1
    
    // MARK: PUBLIC
    
    public func tap() {
        pveTaps += 1
    }
    
    public func endTraining() {
        state = .over
        
        view?.endTraining()
    }
    
    public func updateTip() {
        if state == .over {
            if view != nil {
                view?.understandTap(view!)
            }
        }
        
        let nextTipValue = nextTip()
        
        view?.updateTip(nextTipValue)
        
        if nextTipValue == nil {
            state = .going
            
            startTimer()
        }
    }

}

// MARK: PRIVATE

extension TrainingController {
    
    private func nextTip() -> String? {
        tip += 1
        
        if tip == 1 {
            view?.moveCameraTo("run")
        }
        
        if view == nil {
            return nil
        }
        
        return view!.tips.count > tip ? view!.tips[tip] : nil
    }
    
    private func startTimer() {
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            if self.state == .going {
                self.tick()
            }
        })
    }
    
    private func tick() {
        // steps
        
        let stepMe = Float(pveTaps) / 3
        
        pveTaps = 0
        
        view?.move(stepMe: stepMe)
    }
    
}
