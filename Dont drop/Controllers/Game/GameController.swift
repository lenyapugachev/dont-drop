//
//  GameController.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 13/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

class GameController {
    
    enum GameState {
        case paused
        case prepare
        case going
        case gameover
    }
    
    var view: GameViewController?
    
    var soundOn = true
    
    var state: GameState = .paused
    var firstTutorialSeen = false
    
    var timer: Timer?
    
    var levels: [Level] = []
    var currentLevel: Int = 0
    var totalPoints: Int = 0
    
    var pveMode = true
    var pveTaps = 0
    
    var pvePoints = 0 {
        didSet {
            view?.pointsLabel.countFrom(fromValue: Float(oldValue), to: Float(pvePoints), withDuration: 1, andAnimationType: .EaseIn, andCountingType: .Int)
        }
    }
    
    var leaderpoints: [(String,Int,Bool)] = []
    
    public func setup() {
        if let _soundOn = UserDefaults.standard.object(forKey: "soundOn") as? Bool {
            soundOn = _soundOn
        }
        
        view?.updateSound()
    }
    
    public func checkTrainingCompleted() -> Bool {
        return UserDefaults.standard.bool(forKey: "trainingCompleted")
    }
    
    public func soundToggle() {
        soundOn = !soundOn
        
        UserDefaults.standard.set(soundOn, forKey: "soundOn")
        
        view?.updateSound()
    }
    
    public func run() {
        state = .prepare
        
        pvePoints = 0
        
        view?.startPrepare()
        
        delay(3, {
            self.state = .going
            
            self.pveTaps = 0
            
            self.startTimer()
        })
    }
    
    public func pause() {
        firstTutorialSeen = true
        
        if state == .gameover {
            view?.restart()
            view?.unpause(menu: true)
            
            return
        }
        
        if state == .paused {
            unpause()
            
            return
        }
        
        state = .paused
        
        view?.pause()
        
        timer?.invalidate()
    }
    
    public func unpause() {
        state = .going
        
        view?.unpause()
        
        startTimer()
    }
    
    public func gameover() {
        firstTutorialSeen = true
        
        state = .gameover
        
        view?.finalPointsLabel?.text = String.localizedStringWithFormat(kLocalePoints, pvePoints)
        
        delay(0.2) {
            self.view?.finalPointsLabel?.text = String.localizedStringWithFormat(kLocalePoints, self.pvePoints)
            
            self.addTotalPoints(self.pvePoints)
            
            Network.shared.sendLeaderboardPoints(points: self.pvePoints, onComplete: { _ in }, onFail: { _ in })
        }
    }
    
    public func tap() {
        pveTaps += 1
    }
    
    public func markTrainingCompleted() {
        UserDefaults.standard.set(true, forKey: "trainingCompleted")
        
        if !firstTutorialSeen {
            view?.start()
        }
    }
    
}

extension GameController {
    
    private func addTotalPoints(_ points: Int) {
        let oldTotal = totalPoints
        
        totalPoints += points
        
        UserDefaults.standard.set(totalPoints, forKey: "totalPoints")
        
        for (i, level) in levels.enumerated() {
            if level.unlockPoints > oldTotal && level.unlockPoints <= totalPoints {
                view?.levelUnlocked(i)
            }
        }
    }
    
    private func startTimer() {
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            if self.state == .going {
                self.tick()
            }
        })
    }
    
    private func tick() {
        if pveMode {
            // PVE:
            
            // steps
            
            let stepMe = Float(pveTaps) / 3
            let stepOpp = Float.random(min: 0, max: 3)
            
            pveTaps = 0
            
            view?.move(stepMe: stepMe, stepOpp: stepOpp)
            
            // points
            
            let points = 200 - Int(abs(stepOpp - stepMe) * 100)
            
            pvePoints += points < 0 ? 0 : (points * levels[currentLevel].difficulty)
        } else {
            // PVP:
            
            
        }
    }
    
}
