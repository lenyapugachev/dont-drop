//
//  Game+leaderboard.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 02/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

extension GameController {
    
    public func getLeaderboard() {
        Network.shared.getLeaderboard(onComplete: { data in
            Network.shared.device(onComplete: { device in
                self.leaderpoints.removeAll()
                
                var minPoints = -1
                
                if let data = data as? [String:Any] {
                    if let points = data["points"] as? [[String:Any]] {
                        for i in 0 ... (points.count - 1) {
                            let point = points[i]
                            
                            if let value = point["points"] as? Int {
                                if value > 0 {
                                    minPoints = minPoints == -1 ? value : min(minPoints, value)
                                    
                                    if let nick = (point["device"] as? [[String:Any]])?.first?["nick"] as? String {
                                        self.leaderpoints.append((nick, value, device.nick == nick))
                                    }
                                }
                            }
                        }
                    }
                    
                    if let mypoints = data["mypoints"] as? [[String:Any]] {
                        for i in 0 ... (mypoints.count - 1) {
                            let point = mypoints[i]
                            
                            if let value = point["points"] as? Int {
                                if value < minPoints || (value == minPoints && device.nick != self.leaderpoints.last?.0) {
                                    self.leaderpoints.append((device.nick, value, true))
                                }
                            }
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.view?.leaderboardTable.reloadData()
                }
            }, onFail: { _ in })
        }, onFail: { _ in })
    }
    
    public func saveNickname(_ nick: String, onComplete: @escaping () -> (), onFail: @escaping (Error) -> ()) {
        Network.shared.saveNick(nick: nick, onComplete: {
            onComplete()
        }, onFail: onFail)
    }
    
}
