//
//  Game+level.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 19.08.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

struct Level {
    
    let name: String
    
    let difficulty: Int
    
    let backgroundColor: UIColor
    let leftPyramid: UIColor
    let leftPyramidSpecular: UIColor
    let rightPyramid: UIColor
    let rightPyramidSpecular: UIColor
    let pole: UIColor
    let poleSpecular: UIColor
    let poleIllumination: UIColor

    let unlockPoints: Int
    
    let animationsType: String
    let animations: [LevelAnimation]
    
}

struct LevelAnimation {
    
    let image: String
    let parameter: Int
    
}

extension GameController {
    
    public func loadLevels() {
        currentLevel = UserDefaults.standard.integer(forKey: "currentLevel")
        totalPoints = UserDefaults.standard.integer(forKey: "totalPoints")
        
        let fileURL = URL(fileURLWithPath: "\(Bundle.main.resourcePath!)/levels.data")
            
        do {
            let string = try String(contentsOf: fileURL, encoding: .utf8)
            
            let levelLines = string.components(separatedBy: "\n")
            
            levels.removeAll()
            
            for line in levelLines {
                let levelLine = line.components(separatedBy: ",")
                
                if levelLine.count != 29 { continue }
                
                var animations: [LevelAnimation] = []
                
                let animationsRaw = levelLine[28].components(separatedBy: "|")
                
                for animation in animationsRaw {
                    let animationLine = animation.components(separatedBy: "/")
                    
                    if animationLine.count != 2 { continue }
                    
                    animations.append(LevelAnimation(image: animationLine[0], parameter: Int(animationLine[1])!))
                }
                
                levels.append(Level(name: levelLine[0], difficulty: Int(levelLine[1])!, backgroundColor: UIColor(red: CGFloat(exactly: Int(levelLine[2])!)! / 255, green: CGFloat(exactly: Int(levelLine[3])!)! / 255, blue: CGFloat(exactly: Int(levelLine[4])!)! / 255, alpha: 1), leftPyramid: UIColor(red: CGFloat(exactly: Int(levelLine[5])!)! / 255, green: CGFloat(exactly: Int(levelLine[6])!)! / 255, blue: CGFloat(exactly: Int(levelLine[7])!)! / 255, alpha: 1), leftPyramidSpecular: UIColor(red: CGFloat(exactly: Int(levelLine[8])!)! / 255, green: CGFloat(exactly: Int(levelLine[9])!)! / 255, blue: CGFloat(exactly: Int(levelLine[10])!)! / 255, alpha: 1), rightPyramid: UIColor(red: CGFloat(exactly: Int(levelLine[11])!)! / 255, green: CGFloat(exactly: Int(levelLine[12])!)! / 255, blue: CGFloat(exactly: Int(levelLine[13])!)! / 255, alpha: 1), rightPyramidSpecular: UIColor(red: CGFloat(exactly: Int(levelLine[14])!)! / 255, green: CGFloat(exactly: Int(levelLine[15])!)! / 255, blue: CGFloat(exactly: Int(levelLine[16])!)! / 255, alpha: 1), pole: UIColor(red: CGFloat(exactly: Int(levelLine[17])!)! / 255, green: CGFloat(exactly: Int(levelLine[18])!)! / 255, blue: CGFloat(exactly: Int(levelLine[19])!)! / 255, alpha: 1), poleSpecular: UIColor(red: CGFloat(exactly: Int(levelLine[20])!)! / 255, green: CGFloat(exactly: Int(levelLine[21])!)! / 255, blue: CGFloat(exactly: Int(levelLine[22])!)! / 255, alpha: 1), poleIllumination: UIColor(red: CGFloat(exactly: Int(levelLine[23])!)! / 255, green: CGFloat(exactly: Int(levelLine[24])!)! / 255, blue: CGFloat(exactly: Int(levelLine[25])!)! / 255, alpha: 1), unlockPoints: Int(levelLine[26])!, animationsType: levelLine[27], animations: animations))
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    public func levelChange(_ level: Int) {
        if levels.count <= level { return }
        
        currentLevel = level
        
        UserDefaults.standard.set(currentLevel, forKey: "currentLevel")
        
        view?.changeLevel(levels[level])
    }
    
    func getRopeWidth() -> CGFloat {
        switch levels[currentLevel].difficulty {
            case 2:
                return 4.6
            case 3:
                return 4.5
            case 4:
                return 4.4
            case 5:
                return 4.3
            default:
                return 4.8
        }
    }

}
