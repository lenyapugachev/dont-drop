//
//  GameView+sound.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 21/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import AVFoundation

let kLocaleSoundButtonOnTitle = NSLocalizedString("sound.on", value: "SOUND ON", comment: "Short title for menu sound ON button")
let kLocaleSoundButtonOffTitle = NSLocalizedString("sound.off", value: "SOUND OFF", comment: "Short title for menu sound OFF button")

extension GameViewController {
    
    public func updateSound() {
        soundButton.setTitle(controller.soundOn ? "🔈  \(kLocaleSoundButtonOnTitle)" : "🔇  \(kLocaleSoundButtonOffTitle)", for: .normal)
        
        player?.volume = controller.soundOn ? 1 : 0
    }
    
}

extension GameViewController {
    
    internal func prepareTheme() {
        guard let url = Bundle.main.url(forResource: "theme", withExtension: "m4a") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.soloAmbient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.m4a.rawValue)
            
            guard let player = player else { return }
            
            player.numberOfLoops = 99
            
            player.prepareToPlay()
        } catch let error {
            print(error.localizedDescription)
        }
    }

    internal func playTheme() {
        guard let player = player else { return }
    
        player.volume = controller.soundOn ? 1 : 0
        
        player.currentTime = 0
        player.play()
        
        beat()
    }
    
    internal func playGameover() {
        guard let player = player else { return }
        
        player.volume = controller.soundOn ? 1 : 0
        
        player.currentTime = 0
        player.play()
        
        showGameoverButtons()
        
        delay(3.9, {
            if self.controller.state != .gameover { return }
            
            player.stop()
        })
    }

}

extension GameViewController {
    
    private func beat() {
        guard let player = player else { return }
        
        if !player.isPlaying { return }
        
        delay(3.99, {
            if self.controller.state == .gameover {
                self.playGameover()
            } else {
                self.beat()
            }
        })
    }
    
}
