//
//  GameView+level.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 18.08.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit
import SceneKit

// MARK: PUBLIC

let kLocaleLevelChoose = NSLocalizedString("level.choose", value: "CHOOSE LEVEL", comment: "Short title for menu header when choosing level")
let kLocaleLevelUnlocked = NSLocalizedString("level.unlocked", value: "LEVEL UNLOCKED", comment: "Short title for menu header when level unlocked")
let kLocaleLevelButtonChoose = NSLocalizedString("level.button.choose", value: "CHOOSE", comment: "Main level button title, when can be activate")
let kLocaleLevelButtonActivated = NSLocalizedString("level.button.activated", value: "ACTIVATED", comment: "Main level button title, when activate")
let kLocaleLevelButtonUnlock = NSLocalizedString("level.button.unlock", value: "UNLOCK", comment: "Main level button title, when should unlock")
let kLocaleLevelDifficultyEasy = NSLocalizedString("level.difficulty.easy", value: "EASY", comment: "Difficulty easy")
let kLocaleLevelDifficultyMedium = NSLocalizedString("level.difficulty.medium", value: "MEDIUM", comment: "Difficulty medium")
let kLocaleLevelDifficultyHard = NSLocalizedString("level.difficulty.hard", value: "HARD", comment: "Difficulty hard")
let kLocaleLevelDifficultyExtreme = NSLocalizedString("level.difficulty.extreme", value: "EXTREME", comment: "Difficulty extreme")
let kLocaleLevelDifficultyPro = NSLocalizedString("level.difficulty.pro", value: "PRO", comment: "Difficulty pro")
let kLocaleLevelDifficultyBeginner = NSLocalizedString("level.difficulty.beginner", value: "BEGINNER", comment: "Difficulty beginner")
let kLocaleLevelPopupTitle = NSLocalizedString("level.popup.title", value: "All levels unlock", comment: "Popup title")
let kLocaleLevelPopupCancel = NSLocalizedString("level.popup.cancel", value: "Cancel", comment: "Popup cancel")
let kLocaleLevelPopupUnlock = NSLocalizedString("level.popup.unlock", value: "Unlock for %@", comment: "Popup unlock")
let kLocaleLevelPopupMessage = NSLocalizedString("level.popup.message", value: "This level is locked, to unlock it you still have %d points to go. Or you can unlock all levels right now just for %@:", comment: "Popup message")
let kLocaleLevelPointsToUnlockTip = NSLocalizedString("level.points.tip", value: "Level will be unlocked after you collect %d more points", comment: "Number of points to collect tip")

extension GameViewController {
    
    public func level(unlock: Bool = false) {
        initiateLevel()
        
        levelLabel.text = unlock ? kLocaleLevelUnlocked : kLocaleLevelChoose
        
        UIView.animate(withDuration: 0.2, animations: {
            self.levelView.alpha = 1
            
            self.pointsLabel.alpha = 0
            self.pauseButton.alpha = 0
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.pauseView.alpha = 0
            })
        })
    }
    
    public func levelClose() {
        UIView.animate(withDuration: 0.2, animations: {
            self.pauseView.alpha = 1
            
            self.pointsLabel.alpha = 1
            self.pauseButton.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.levelView.alpha = 0
            }, completion: nil)
        })
    }
    
    public func changeLevel(_ level: Level) {
        let animation: CABasicAnimation = CABasicAnimation(keyPath: "colors")
        
        let toColors = [level.backgroundColor.cgColor, level.backgroundColor.darker(by: 15)!.cgColor]

        animation.fromValue = currentColors
        animation.toValue = toColors
        animation.duration = 0.5
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        currentColors = toColors

        backgroundView.layer.sublayers?.last?.add(animation, forKey: "animateGradient")
        
        UIView.animate(withDuration: 0.2, animations: {
            self.backgroundAnimationsView.alpha = 0
        }, completion: { _ in
            self.backgroundAnimationsView.subviews.forEach { $0.removeFromSuperview() }
            
            self.startBackgroundAnimations()
            
            UIView.animate(withDuration: 0.2, animations: {
                self.backgroundAnimationsView.alpha = 1
            })
        })
        
        restartButton.setTitleColor(level.backgroundColor, for: .normal)
        
        me?.geometry?.materials = [Material(diffuseColor: level.leftPyramid, specularColor: level.leftPyramidSpecular)]
        opp?.geometry?.materials = [Material(diffuseColor: level.rightPyramid, specularColor: level.rightPyramidSpecular)]
        rope?.geometry?.materials = [Material(diffuseColor: level.pole, specularColor: level.poleSpecular, illuminationColor: level.poleIllumination)]
        
        rope?.scale.x = Float(controller.getRopeWidth())
    }
    
    public func levelUnlocked(_ index: Int) {
        level(unlock: true)
        
        levelCollection.setContentOffset(CGPoint(x: CGFloat(index) * view.frame.size.width, y: 0), animated: true)
        
        confettiView.type = .star
        
        confettiView.startConfetti()
        
        delay(2, {
            self.confettiView.stopConfetti()
        })
    }
    
}

// MARK: INTERNAL

extension GameViewController {
    
    internal func setupLevel() {
        levelView.alpha = 0
        
        levelView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.95).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
        
        let layout = LevelLayout()
        
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: view.frame.width, bottom: 0, right: view.frame.width)
        
        levelCollection.collectionViewLayout = layout
        
        controller.loadLevels()
    }
    
    internal func initiateLevel() {
        levelCollection.contentOffset.x = view.frame.width * CGFloat(controller.currentLevel)
        
        levelCollection.reloadData()
        
        scrollViewDidEndDecelerating(levelCollection)
        
        controller.levelChange(controller.currentLevel)
        
        startBackgroundAnimations()
    }
    
}

// MARK: UICOLLECTIONVIEW

extension GameViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.levels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! LevelCell
        
        let level = controller.levels[indexPath.row]
        
        let levelUnlocked = level.unlockPoints <= controller.totalPoints
        
        cell.name.text = level.name
        
        cell.back.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        
        cell.back.gradient(colors: [
            level.backgroundColor.cgColor,
            levelUnlocked ? level.backgroundColor.darker(by: 15)!.cgColor : level.backgroundColor.lighter(by: 100)!.withAlphaComponent(0.5).cgColor
        ])
        
        cell.chooseButton.setTitleColor(level.backgroundColor, for: .normal)
        
        cell.chooseButton.isEnabled = indexPath.row != controller.currentLevel
        cell.chooseButton.alpha = cell.chooseButton.isEnabled ? 1 : 0.75
        
        cell.chooseButton.setTitle(indexPath.row != controller.currentLevel ? (levelUnlocked ? kLocaleLevelButtonChoose : kLocaleLevelButtonUnlock) : kLocaleLevelButtonActivated, for: .normal)
        
        cell.backWidth.constant = view.frame.width / 1.75
        
        cell.centerC.constant = 0
        
        switch level.difficulty {
            case 2:
                cell.difficulty.text = kLocaleLevelDifficultyEasy
            break
            case 3:
                cell.difficulty.text = kLocaleLevelDifficultyMedium
            break
            case 4:
                cell.difficulty.text = kLocaleLevelDifficultyHard
            break
            case 5:
                cell.difficulty.text = kLocaleLevelDifficultyExtreme
            break
            case 6:
                cell.difficulty.text = kLocaleLevelDifficultyPro
            break
            default:
                cell.difficulty.text = kLocaleLevelDifficultyBeginner
            break
        }
        
        if level.animations.count > 0 {
            cell.icon.image = UIImage(named: level.animations[0].image)
        } else {
            cell.icon.image = nil
        }
        
        cell.lock.isHidden = levelUnlocked
        cell.unlock.isHidden = levelUnlocked
        
        cell.unlock.text = String.localizedStringWithFormat(kLocaleLevelPointsToUnlockTip, level.unlockPoints - self.controller.totalPoints)
        
        cell.choose = {
            if levelUnlocked {
                self.controller.levelChange(indexPath.row)
                
                self.levelCollection.reloadData()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.levelClose()
                })
            } else {
                let sum = "99 RUB"
                
                let alertController = UIAlertController(title: kLocaleLevelPopupTitle, message: String.localizedStringWithFormat(kLocaleLevelPopupMessage, level.unlockPoints - self.controller.totalPoints, sum), preferredStyle: .actionSheet)
                
                let unlock = UIAlertAction(title: String.localizedStringWithFormat(kLocaleLevelPopupUnlock, sum), style: .default) { _ in
                    
                }
                
                alertController.addAction(unlock)
                
                let cancel = UIAlertAction(title: kLocaleLevelPopupCancel, style: .cancel) { _ in }
                
                alertController.addAction(cancel)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: collectionView.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell in levelCollection.visibleCells {
            if let cell = cell as? LevelCell {
                if cell.centerC.constant != 0 {
                    cell.centerC.constant = 0
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        cell.layoutIfNeeded()
                    })
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let delta = x.truncatingRemainder(dividingBy: view.frame.width)
        let index = Int((x - delta) / view.frame.width)
        
        let leftIndex = index - 1
        let rightIndex = index + 1
        
        if let cell = levelCollection.cellForItem(at: IndexPath(item: leftIndex, section: 0)) as? LevelCell {
            cell.centerC.constant = (view.frame.width - delta) / 4
            
            UIView.animate(withDuration: 0.5, animations: {
                cell.layoutIfNeeded()
            })
        }
        
        if let cell = levelCollection.cellForItem(at: IndexPath(item: rightIndex, section: 0)) as? LevelCell {
            cell.centerC.constant = -(view.frame.width - delta) / 4
            
            UIView.animate(withDuration: 0.5, animations: {
                cell.layoutIfNeeded()
            })
        }
    }
    
}

class LevelLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        guard let collectionView = self.collectionView else {
            let latestOffset = super.targetContentOffset(forProposedContentOffset: proposedContentOffset, withScrollingVelocity: velocity)
            return latestOffset
        }

        let pageWidth = collectionView.frame.size.width / 3
        let approximatePage = collectionView.contentOffset.x / pageWidth
        let currentPage = velocity.x == 0 ? round(approximatePage) : (velocity.x < 0.0 ? floor(approximatePage) : ceil(approximatePage))
        let flickVelocity = velocity.x * 0.4
        let flickedPages = (abs(round(flickVelocity)) <= 1) ? 0 : round(flickVelocity)
        let newHorizontalOffset = ((currentPage + flickedPages) * pageWidth) - collectionView.contentInset.left

        return CGPoint(x: newHorizontalOffset, y: proposedContentOffset.y)
    }
    
}

class LevelCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var difficulty: UILabel!
    @IBOutlet weak var unlock: UILabel!
    
    @IBOutlet weak var back: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lock: UIImageView!
    
    @IBOutlet weak var chooseButton: UIButton!
    
    @IBOutlet weak var backWidth: NSLayoutConstraint!
    @IBOutlet weak var centerC: NSLayoutConstraint!
    
    @IBAction func chooseTap(_ sender: Any) {
         choose()
    }
    
    @IBAction func chooseDown(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            (sender as! UIButton).transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        })
    }
    
    @IBAction func chooseUp(_ sender: Any) {
         UIView.animate(withDuration: 0.2, animations: {
             (sender as! UIButton).transform = CGAffineTransform(scaleX: 1, y: 1)
         })
    }
    
    var choose: () -> () = {}
    
}
