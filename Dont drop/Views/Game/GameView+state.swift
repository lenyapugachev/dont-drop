//
//  GameView+state.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 20/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

let kLocaleVictoryMessage = NSLocalizedString("victory", value: "VICTORY", comment: "Victory short message")
let kLocaleGameoverMessage = NSLocalizedString("gameover", value: "GAME OVER", comment: "Game over short message")
let kLocalePoints = NSLocalizedString("points", value: "%d POINTS", comment: "Number of points")

// MARK: PUBLIC

extension GameViewController {
    
    public func startPrepare() {
        playTheme()
        
        prepare?.geometry = generateTextGeometry(withText: "3")
        prepare?.center()
        
        prepare?.runAction(SCNAction.fadeIn(duration: 0.5))
        prepare?.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(GLKMathDegreesToRadians(360)), z: 0, duration: 1))
        
        delay(0.6, {
            self.prepare?.geometry = self.generateTextGeometry(withText: "2")
            self.prepare?.center()
        })
        
        delay(1, {
            self.prepare?.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(GLKMathDegreesToRadians(360)), z: 0, duration: 1))
        })
        
        delay(1.6, {
            self.prepare?.geometry = self.generateTextGeometry(withText: "1")
            self.prepare?.center()
        })
        
        delay(2, {
            self.prepare?.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(GLKMathDegreesToRadians(360)), z: 0, duration: 1))
        })
        
        delay(2.4, {
            self.prepare?.geometry = self.generateTextGeometry(withText: "GO")
            self.prepare?.center()
        })
        
        delay(10, {
            if self.controller.state == .going {
                self.prepare?.runAction(SCNAction.fadeOut(duration: 0.5))
            }
        })
    }
    
}

// MARK: INTERNAL

extension GameViewController {

    internal func setupState() {
        restartButton.setTitleColor(view.backgroundColor!, for: .normal)
        
        gameoverView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.5).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
    }
    
    internal func setupPrepare() {
        let minimum = float3(prepare!.boundingBox.min)
        let maximum = float3(prepare!.boundingBox.max)
        
        let translation = (maximum - minimum) * 0.5
        
        prepare?.pivot = SCNMatrix4MakeTranslation(translation.x, translation.y, translation.z)
        
        prepare?.center()
    }
    
    internal func setupConfetti() {
        confettiView.type = .confetti
        confettiView.intensity = 1
    }
    
    internal func restart() {
        if restartBlocked { return }
        
        moveCameraTo("initial", yRotation: 0.3)
        
        me?.runAction(SCNAction.move(to: SCNVector3(x: -2, y: 0.02, z: 0), duration: 1.0))
        opp?.runAction(SCNAction.move(to: SCNVector3(x: 2, y: 0.02, z: 0), duration: 1.0))
        
        UIView.animate(withDuration: 0.5, animations: {
            self.sceneView.alpha = 0
        })
        
        UIView.animate(withDuration: 0.2, animations: {
            self.gameoverView.alpha = 0
        })
        
        delay(1, {
            UIView.animate(withDuration: 1, animations: {
                self.sceneView.alpha = 1
            })
            
            self.start()
        })
    }
    
    internal func start() {
        rope?.physicsBody?.type = .kinematic
        
        moveCameraTo("run", onComplete: {
            self.controller.run()
        })
        
        rope()
        
        delay(4, {
            UIView.animate(withDuration: 1, animations: {
                self.pointsLabel.alpha = 1
                self.pauseButton.alpha = 1
            })
        })
    }
    
    internal func gameover(win: Bool = false) {
        controller.gameover()
        
        restartBlocked = true
        
        restartButton.alpha = 0
        menuButton.alpha = 0
        
        if !win { rope?.physicsBody?.type = .dynamic }
        else {
            confettiView.type = .confetti
            
            confettiView.startConfetti()
            
            delay(15, {
                self.confettiView.stopConfetti()
            })
        }
        
        prepare?.runAction(SCNAction.fadeOut(duration: 0.25))
        
        if #available(iOS 11.1, *) {
            gameoverLabel?.text = win ? "\(kLocaleVictoryMessage) 🎉🏆" : "\(kLocaleGameoverMessage) 🤬"
        } else {
            gameoverLabel?.text = win ? "\(kLocaleVictoryMessage) 🎉🏆" : "\(kLocaleGameoverMessage) 😬"
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.gameoverView.alpha = 1
            
            self.pointsLabel.alpha = 0
            self.pauseButton.alpha = 0
        })
    }
    
    internal func showGameoverButtons() {
        restartBlocked = false
        
        UIView.animate(withDuration: 0.3, animations: {
            self.restartButton.alpha = 1
            self.menuButton.alpha = 1
        })
    }
    
}

// MARK: PRIVATE

extension GameViewController {
    
    private func generateTextGeometry(withText: String) -> SCNText {
        let textGeometry = SCNText(string: withText, extrusionDepth: 10)
        
        textGeometry.font = UIFont(name: "Avenir-Black", size: 140)
        
        return textGeometry
    }
    
}
