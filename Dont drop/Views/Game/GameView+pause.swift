//
//  GameView+pause.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 21/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: PUBLIC

let kLocaleTopRestartButtonTitle = NSLocalizedString("top.restart", value: "RESTART", comment: "Short title for small top restart button")
let kLocaleTopRestartButtonOnPauseTitle = NSLocalizedString("top.pause", value: "PLAY", comment: "Short title for small top unpause button")
let kLocaleMenuHeaderTitle = NSLocalizedString("menu.menu", value: "MENU", comment: "Short title for menu header")
let kLocaleMenuHeaderOnPauseTitle = NSLocalizedString("menu.pause", value: "PAUSED", comment: "Short title for menu header while pause")

extension GameViewController {
    
    public func pause(menu: Bool = false) {
        pauseButton.setImage(UIImage(named: "play"), for: .normal)
        pauseButton.setTitle(menu ? "  \(kLocaleTopRestartButtonTitle)" : "  \(kLocaleTopRestartButtonOnPauseTitle)", for: .normal)
        
        pauseLabel.text = menu ? kLocaleMenuHeaderTitle : kLocaleMenuHeaderOnPauseTitle
        
        UIView.animate(withDuration: 0.2, animations: {
            self.gameoverView.alpha = 0
            self.pauseView.alpha = 1
            
            self.pointsLabel.alpha = 1
            self.pauseButton.alpha = 1
        })
    }
    
    public func unpause(menu: Bool = false) {
        pauseButton.setImage(UIImage(named: "pause"), for: .normal)
        pauseButton.setTitle("", for: .normal)
        
        UIView.animate(withDuration: 0.2, animations: {
            self.pauseView.alpha = 0
            self.leaderboardView.alpha = 0
            self.chooseNameView.alpha = 0
            
            if menu {
                self.pointsLabel.alpha = 0
                self.pauseButton.alpha = 0
            }
        })
    }
    
}

// MARK: INTERNAL

extension GameViewController {
    
    internal func setupPause() {
        pauseButton.alpha = 0
        pointsLabel.alpha = 0
        
        pauseView.alpha = 0
        
        pauseView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.95).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
    }
    
}
