//
//  GameView+background.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 18.08.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

extension GameViewController {
    
    func startBackgroundAnimations() {
        for animation in controller.levels[controller.currentLevel].animations {
            switch controller.levels[controller.currentLevel].animationsType {
                case "ORBIT":
                    startOrbit(imageName: animation.image, orbit: CGFloat(exactly: animation.parameter)!, duration: Double.random(in: 6..<12))
                break
                default: break
            }
        }
    }
    
    private func startOrbit(imageName: String, orbit: CGFloat, duration: CFTimeInterval = 10) {
        let circlePath = UIBezierPath(arcCenter: view.center.applying(CGAffineTransform(translationX: 0, y: 100)), radius: view.frame.width + orbit, startAngle: 0, endAngle: .pi*2, clockwise: true)

        let animation = CAKeyframeAnimation(keyPath: #keyPath(CALayer.position))
        animation.duration = duration
        animation.repeatCount = MAXFLOAT
        animation.path = circlePath.cgPath

        let squareView = UIImageView()
        squareView.frame = CGRect(x: 0, y: 0, width: imageName == "star" ? 20 : 50, height: imageName == "star" ? 20 : 50)
        squareView.image = UIImage(named: imageName)
        
        backgroundAnimationsView.addSubview(squareView)
        
        squareView.layer.add(animation, forKey: nil)
    }
    
}
