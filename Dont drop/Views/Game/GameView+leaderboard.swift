//
//  GameView+leaderboard.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 01/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: PUBLIC

//let kLocaleTopRestartButtonTitle = NSLocalizedString("top.restart", value: "RESTART", comment: "Short title for small top restart button")

extension GameViewController {
    
    public func leaderboard() {
        UIView.animate(withDuration: 0.2, animations: {
            self.leaderboardView.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.pauseView.alpha = 0
            })
        })
        
        controller.getLeaderboard()
    }
    
    public func leaderboardClose() {
        UIView.animate(withDuration: 0.2, animations: {
            self.pauseView.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.leaderboardView.alpha = 0
            }, completion: { _ in
                self.chooseNameView.alpha = 0
            })
        })
    }
    
    public func leaderboardChooseName() {
        Network.shared.device(onComplete: { device in
            self.chooseNameField.text = device.nick
            
            UIView.animate(withDuration: 0.2, animations: {
                self.chooseNameView.alpha = 1
            }, completion: { _ in
                UIView.animate(withDuration: 0.2, animations: {
                    self.leaderboardView.alpha = 0
                })
            })
        }, onFail: { _ in })
    }
    
    public func leaderboardChooseNameClose() {
        UIView.animate(withDuration: 0.2, animations: {
            self.leaderboardView.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.2, animations: {
                self.chooseNameView.alpha = 0
            })
        })
    }
    
    public func leaderboardChooseNameSave() {
        chooseNameSaveButton.isEnabled = false
        
        view.endEditing(true)
        
        chooseNameField.isUserInteractionEnabled = false
        
        controller.saveNickname(chooseNameField.text!, onComplete: {
            DispatchQueue.main.async {
                self.leaderboardChooseNameClose()
                
                self.controller.getLeaderboard()
                
                self.chooseNameField.isUserInteractionEnabled = true
                self.chooseNameSaveButton.isEnabled = true
            }
        }, onFail: { error in
            if let errorApi = (error as NSError).userInfo["error"] as? String {
                if errorApi == "nick_existing" {
                    DispatchQueue.main.async {
                        UIView.animate(withDuration: 0.2, animations: {
                            self.chooseNameExisting.alpha = 1
                        })
                        
                        self.chooseNameField.becomeFirstResponder()
                    }
                }
            }
            
            DispatchQueue.main.async {
                self.chooseNameField.isUserInteractionEnabled = true
                self.chooseNameSaveButton.isEnabled = true
            }
        })
    }
    
}

// MARK: INTERNAL

extension GameViewController {
    
    internal func setupLeaderboard() {
        leaderboardView.alpha = 0
        chooseNameView.alpha = 0
        
        leaderboardView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.95).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
        
        chooseNameView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.95).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
        
        leaderboardTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 100))
    }
    
}

// MARK: UITABLEVIEW

extension GameViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return controller.leaderpoints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        
        cell.subviews.forEach { $0.removeFromSuperview() }
        
        let nick = UILabel(frame: CGRect(x: 30, y: 0, width: view.frame.width - 60, height: 50))
        nick.text = "\(indexPath.row + 1). \(controller.leaderpoints[indexPath.row].0)"
        nick.font = UIFont(name: "AvenirNextCondensed-Bold", size: 20)
        nick.textColor = controller.leaderpoints[indexPath.row].2 ? .yellow : .white
        
        cell.addSubview(nick)
        
        let points = UILabel(frame: CGRect(x: 30, y: 0, width: view.frame.width - 60, height: 50))
        points.text = "\(controller.leaderpoints[indexPath.row].1)"
        points.textColor = controller.leaderpoints[indexPath.row].2 ? .yellow : .white
        points.font = UIFont(name: "AvenirNextCondensed-Bold", size: 20)
        points.textAlignment = .right
        
        cell.addSubview(points)
        
        return cell
    }
    
}

// MARK: UITEXTFIELD

extension GameViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        leaderboardChooseNameSave()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            
            chooseNameSaveButton.isEnabled = updatedText.count >= 5
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.chooseNameExisting.alpha = 0
        })
        
        return true
    }
    
}
