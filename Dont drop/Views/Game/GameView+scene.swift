//
//  GameView+scene.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 13/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

extension GameViewController {
    
    public func move(stepMe: Float, stepOpp: Float) {
        if controller.state != .going { return }
        
        var mePos = me?.position
        var oppPos = opp?.position
        
        mePos?.y = 1
        oppPos?.y = 1
        
        mePos?.z -= stepMe
        oppPos?.z -= stepOpp
        
        if stepMe != 0 {
            me?.runAction(SCNAction.move(by: SCNVector3(x: 0, y: 0, z: -stepMe), duration: 0.2))
            me?.runAction(SCNAction.rotateBy(x: 0, y: -18, z: 0, duration: 0.2))
        }
        
        if stepOpp != 0 {
            opp?.runAction(SCNAction.move(by: SCNVector3(x: 0, y: 0, z: -stepOpp), duration: 0.2))
            opp?.runAction(SCNAction.rotateBy(x: 0, y: 18, z: 0, duration: 0.2))
        }
        
        if mePos!.z < -70 && oppPos!.z < -70 {
            gameover(win: true)
        }
        
        rope(mePos: mePos, oppPos: oppPos)
        
        slideCameraTo(max(mePos?.z ?? 0, oppPos?.z ?? 0) + 12)
    }
    
}

extension GameViewController {
    
    internal func setupScene() {
        scene = SCNScene(named: "Scenes.scnassets/Game.scn")!
        
        scene.background.contents = UIColor.clear
        
        me = scene.rootNode.childNode(withName: "left", recursively: true)!
        opp = scene.rootNode.childNode(withName: "right", recursively: true)!
        rope = scene.rootNode.childNode(withName: "rope", recursively: true)!
        prepare = scene.rootNode.childNode(withName: "prepare", recursively: true)!
        
        setupCamera()
        setupSceneView()
        
        setupRope()
        setupPrepare()
        setupLight()
    }
    
    internal func setupSceneView() {
        currentColors = [
            view.backgroundColor!.cgColor,
            view.backgroundColor!.darker(by: 15)!.cgColor
        ]
        
        backgroundView.gradient(colors: currentColors)
        
        sceneView.scene = scene
        
        sceneView.backgroundColor = .clear
        
        sceneView.allowsCameraControl = false
        sceneView.showsStatistics = false
        
//        sceneView.debugOptions = [.showPhysicsShapes]
        
        sceneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(_:))))
    }
    
    internal func setupRope() {
        let ropeBody = SCNPhysicsBody(type: .dynamic, shape: SCNPhysicsShape(geometry: SCNBox(width: 4.4, height: 0.2, length: 0.2, chamferRadius: 0), options: nil))
        
        rope?.physicsBody = ropeBody
    }
    
    internal func setupLight() {
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
    }
    
    internal func rope(mePos: SCNVector3? = nil, oppPos: SCNVector3? = nil) {
        var mePos = mePos != nil ? mePos : me?.position
        var oppPos = oppPos != nil ? oppPos : opp?.position
        
        mePos?.y = 1
        oppPos?.y = 1
        
        let ropePosition = SCNVector3(x: (mePos!.x + oppPos!.x) / 2, y: 1.12, z: (mePos!.z + oppPos!.z) / 2)
        let ropeAngle = oppPos!.angleBetweenVectors(mePos!)
        let ropeDistance = oppPos!.distanceBetweenVectors(mePos!)
        
        if ropeDistance > (controller.getRopeWidth() - 0.1) {
            gameover()
        }
        
        rope?.runAction(SCNAction.move(to: ropePosition, duration: 0.2))
        rope?.runAction(SCNAction.rotateTo(x: 0, y: ropeAngle, z: 0, duration: 0.2))
    }
    
}
