//
//  GameViewController.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 12/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import SpriteKit
import AVFoundation
import SwiftConfettiView

class GameViewController: UIViewController {
    
    // MARK: OUTLETS
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backgroundAnimationsView: UIView!
    @IBOutlet weak var sceneView: SCNView!
    
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var pointsLabel: SACountingLabel!
    
    @IBOutlet weak var pauseView: UIView!
    @IBOutlet weak var pauseLabel: UILabel!
    @IBOutlet weak var soundButton: UIButton!
    
    @IBOutlet weak var leaderboardView: UIView!
    @IBOutlet weak var leaderboardTable: UITableView!
    @IBOutlet weak var chooseNameView: UIView!
    @IBOutlet weak var chooseNameField: UITextField!
    @IBOutlet weak var chooseNameSaveButton: UIButton!
    @IBOutlet weak var chooseNameExisting: UILabel!
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelView: UIView!
    @IBOutlet weak var levelCollection: UICollectionView!
    
    @IBOutlet weak var gameoverView: UIView!
    @IBOutlet weak var gameoverLabel: UILabel!
    @IBOutlet weak var finalPointsLabel: SACountingLabel!
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var confettiView: SwiftConfettiView!
    
    // MARK: ACTIONS
    
    @IBAction func restartTap(_ sender: Any) {
        restart()
    }
    
    @objc func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        controller.tap()
    }
    
    @IBAction func pauseTap(_ sender: Any) {
        controller.pause()
    }
    
    @IBAction func menuTap(_ sender: Any) {
        pause(menu: true)
    }
    
    @IBAction func menuLevelTap(_ sender: Any) {
        level()
    }
    
    @IBAction func menuLevelCloseTap(_ sender: Any) {
        levelClose()
    }
    
    @IBAction func menuSoundTap(_ sender: Any) {
        controller.soundToggle()
    }
    
    @IBAction func menuLeaderboardTap(_ sender: Any) {
        leaderboard()
    }
    
    @IBAction func menuLeaderboardCloseTap(_ sender: Any) {
        leaderboardClose()
    }
    
    @IBAction func menuLeaderboardNameTap(_ sender: Any) {
        leaderboardChooseName()
    }
    
    @IBAction func menuLeaderboardNameCancelTap(_ sender: Any) {
        leaderboardChooseNameClose()
    }
    
    @IBAction func menuLeaderboardNameSaveTap(_ sender: Any) {
        leaderboardChooseNameSave()
    }
    
    @IBAction func menuTrainingTap(_ sender: Any) {
        performSegue(withIdentifier: "training", sender: nil)
    }
    
    // MARK: MAIN
    
    internal let controller = GameController()
    
    internal var player: AVAudioPlayer?
    internal var restartBlocked = false
    
    internal var scene: SCNScene!
    
    internal var currentColors: [CGColor] = []
    
    internal let cameraNode = SCNNode()
    
    internal var me: SCNNode?
    internal var opp: SCNNode?
    internal var rope: SCNNode?
    internal var prepare: SCNNode?
    
    internal let cameraPositions: [String:SCNVector3] = [
        "initial": SCNVector3(x: 0, y: 0, z: 150),
        "run": SCNVector3(x: 0, y: 6, z: 12)
    ]
    
    // MARK: UICONTROLLERVIEW DELEGATE

    override func viewDidLoad() {
        super.viewDidLoad()
        
        controller.view = self
        
        controller.setup()
        
        prepareTheme()
        
        setupState()
        setupScene()
        setupPause()
        setupLeaderboard()
        setupLevel()
        
        setupConfetti()
        
        delay(1, {
            if self.controller.checkTrainingCompleted() {
                self.start()
            } else {
                self.performSegue(withIdentifier: "training", sender: nil)
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        initiateLevel()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

} 
