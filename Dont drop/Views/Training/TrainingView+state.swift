//
//  TrainingView+state.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

let kLocaleTrainingOverMessage = NSLocalizedString("training.over", value: "A TRAINING IS OVER, LET'S SAY IT WAS A NICE JOB, BUT BE PREPARED TO REAL WORLD", comment: "Training over message")

// MARK: PUBLIC

extension TrainingViewController {
    
    public func endTraining() {
        tipLabel.text = kLocaleTrainingOverMessage
        
        UIView.animate(withDuration: 0.2, animations: {
            self.tipView.alpha = 1
        })
        
        delay(10, {
            self.understandTap(self)
        })
    }
    
}

// MARK: INTERNAL

extension TrainingViewController {
    
    internal func showUnderstandButton() {
        UIView.animate(withDuration: 0.3, animations: {
            self.understandButton.alpha = 1
        })
    }
    
}

// MARK: INTERNAL

extension TrainingViewController {
    
    internal func setupState() {
        understandButton.alpha = 0
    }
    
}
