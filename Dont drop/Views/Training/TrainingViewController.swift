//
//  TrainingViewController.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit
import SpriteKit
import AVFoundation

let kLocaleTraining1Message = NSLocalizedString("training.message1", value: "CONTROLS IN THIS GAME ARE JUST PIECE OF A CAKE", comment: "Training message #1")
let kLocaleTraining2Message = NSLocalizedString("training.message2", value: "JUST TAP ANYWHERE TO MOVE THE LEFT PYRAMID,\n MORE YOU TAP — FASTER YOU MOVE", comment: "Training message #2")
let kLocaleTraining3Message = NSLocalizedString("training.message3", value: "POINT IS NOT TO DROP A FREIGHT WITH YOUR BUDDY ON THE RIGHT WHO IS MOVING DESPERATELY RANDOM", comment: "Training message #3")
let kLocaleTraining4Message = NSLocalizedString("training.message4", value: "BUT NOW TRY TO MOVE SOLO, FEEL THE TAP", comment: "Training message #4")

class TrainingViewController: UIViewController {
    
    // MARK: OUTLETS
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var sceneView: SCNView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var understandButton: UIButton!
    
    @IBOutlet weak var tipView: UIView!
    @IBOutlet weak var tipLabel: UILabel!
    
    // MARK: ACTIONS
    
    @objc func handleTap(_ gestureRecognize: UIGestureRecognizer) {
        controller.tap()
    }
    
    @IBAction func understandTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            if let gameController = UIApplication.shared.keyWindow?.rootViewController as? GameViewController {
                gameController.controller.markTrainingCompleted()
            }
        })
    }
    
    @IBAction func okTap(_ sender: Any) {
        controller.updateTip()
    }
    
    // MARK: MAIN
    
    internal var controller: TrainingController!
    
    internal weak var scene: SCNScene!
    internal weak var cameraNode: SCNNode!
    
    internal weak var me: SCNNode?
    
    internal let cameraPositions: [String:SCNVector3] = [
        "initial": SCNVector3(x: 0, y: 12, z: 12),
        "run": SCNVector3(x: 0, y: 6, z: 12)
    ]
    
    public var tips = [
        kLocaleTraining1Message,
        kLocaleTraining2Message,
        kLocaleTraining3Message,
        kLocaleTraining4Message
    ]
    
    // MARK: UICONTROLLERVIEW DELEGATE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let controller = TrainingController()
        
        self.controller = controller
        
        controller.view = self
        
        setupState()
        setupScene()
        
        controller.updateTip()
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
}
