//
//  TrainingView+camera.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

extension TrainingViewController {
    
    internal func setupCamera() {
        let camera = SCNNode()
        
        cameraNode = camera
        
        cameraNode.camera = SCNCamera()
        scene.rootNode.addChildNode(cameraNode)
        
        cameraNode.position = cameraPositions["initial"]!
        cameraNode.eulerAngles.x = 0.3
    }
    
    internal func moveCameraTo(_ position: String, yRotation: CGFloat = 0, duration: Double = 1.0, onComplete: @escaping () -> () = {}) {
        if let position = cameraPositions[position] {
            cameraNode.runAction(SCNAction.move(to: position, duration: duration))
            cameraNode.runAction(SCNAction.rotateTo(x: 0, y: yRotation, z: 0, duration: duration))
        }
        
        delay(duration, {
            onComplete()
        })
    }
    
    internal func slideCameraTo(_ z: Float) {
        if var position = cameraPositions["run"] {
            position.z = z
            
            cameraNode.runAction(SCNAction.move(to: position, duration: 0.25))
        }
    }
    
}

