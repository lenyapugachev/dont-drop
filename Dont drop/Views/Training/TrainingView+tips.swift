//
//  TrainingView+tips.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

extension TrainingViewController {
    
    public func updateTip(_ tip: String?) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tipView.alpha = 0
        }, completion: { _ in
            if tip != nil {
                self.tipLabel.text = tip
    
                UIView.animate(withDuration: 0.2, animations: {
                    self.tipView.alpha = 1
                })
            }
        })
    }
    
}
