//
//  TrainingView+scene.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 24/03/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import SceneKit

extension TrainingViewController {
    
    public func move(stepMe: Float) {
        if controller?.state != .going { return }
        
        var mePos = me?.position
        
        mePos?.y = 1
        
        mePos?.z -= stepMe
        
        if stepMe != 0 {
            me?.runAction(SCNAction.move(by: SCNVector3(x: 0, y: 0, z: -stepMe), duration: 0.2))
        }
        
        if mePos!.z < -5 {
            showUnderstandButton()
        }
        
        if mePos!.z < -30 {
            controller.endTraining()
        }
        
        slideCameraTo((mePos?.z ?? 0) + 12)
    }
    
}

extension TrainingViewController {
    
    internal func setupScene() {
        let scene = SCNScene(named: "Scenes.scnassets/Training.scn")!
        
        self.scene = scene
        
        scene.background.contents = UIColor.clear
        
        me = scene.rootNode.childNode(withName: "left", recursively: true)!
        
        setupCamera()
        setupSceneView()
        
        setupLight()
        
        startTopAnimation()
    }
    
    internal func startTopAnimation() {
        scene.rootNode.childNode(withName: "top", recursively: true)?.runAction(SCNAction.rotateBy(x: 0, y: CGFloat(Double.pi), z: 0, duration: 2), completionHandler: {
            self.startTopAnimation()
        })
    }
    
    internal func setupSceneView() {
        backgroundView.gradient(colors: [
            view.backgroundColor!.cgColor,
            view.backgroundColor!.darker(by: 15)!.cgColor
        ])
        
        topView.gradient(colors: [
            UIColor.black.withAlphaComponent(0.5).cgColor,
            UIColor.black.withAlphaComponent(0).cgColor
        ])
        
        tipView.gradient(colors: [
            UIColor.black.withAlphaComponent(0).cgColor,
            UIColor.black.withAlphaComponent(1).cgColor
        ])
        
        sceneView.scene = scene
        
        sceneView.backgroundColor = .clear
        
        sceneView.allowsCameraControl = false
        sceneView.showsStatistics = false
        
        //        sceneView.debugOptions = [.showPhysicsShapes]
        
        sceneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(_:))))
    }
    
    internal func setupLight() {
        let lightNode = SCNNode()
        lightNode.light = SCNLight()
        lightNode.light!.type = .omni
        lightNode.position = SCNVector3(x: 0, y: 10, z: 10)
        scene.rootNode.addChildNode(lightNode)
        
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.darkGray
        scene.rootNode.addChildNode(ambientLightNode)
    }
    
}
