//
//  Network+secret.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 02/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation
import CommonCrypto

extension Network {
    
    static public func generateSecret() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dMyHm"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        
        return sha512Hex(string: "\(formatter.string(from: Date()))CQyMpDv7IYDl5eb8Gpdy0n68PxARLpSW8SOnOg7xL7lGYfkwp1glsRglRX3wXxQCFsR5z4NKNTLOsJcph46U59")
    }
    
    static private func sha512Hex(string: String) -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        if let data = string.data(using: String.Encoding.utf8) {
            let value =  data as NSData
            CC_SHA512(value.bytes, CC_LONG(data.count), &digest)
            
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_SHA512_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        
        return digestHex
    }
    
}
