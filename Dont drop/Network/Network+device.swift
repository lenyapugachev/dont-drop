//
//  Network+device.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 02/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import UIKit

struct Device {
    let id: String
    let name: String
    let nick: String
}

extension Network {
    
    public func saveNick(nick: String, onComplete: @escaping () -> (), onFail: @escaping (Error) -> ()) {
        if let deviceNick = UserDefaults.standard.string(forKey: "deviceNick") {
            if nick == deviceNick {
                onComplete()
                
                return
            }
        }
        
        device(onComplete: { device in
            Network.POST("device", data: [
                "deviceId": device.id,
                "name": device.name,
                "nick": nick,
                "secret": Network.generateSecret()
            ], onComplete: { data in
                    UserDefaults.standard.set(nick, forKey: "deviceNick")
                    
                    onComplete()
            }, onFail: onFail)
        }, onFail: onFail)
    }
    
    internal func device(onComplete: @escaping (Device) -> (), onFail: @escaping (Error) -> ()) {
        if let deviceId = UserDefaults.standard.string(forKey: "deviceId") {
            if let deviceName = UserDefaults.standard.string(forKey: "deviceName") {
                if let deviceNick = UserDefaults.standard.string(forKey: "deviceNick") {
                    onComplete(Device(id: deviceId, name: deviceName, nick: deviceNick))
                    
                    return
                }
            }
        }
        
        let deviceId = UUID().uuidString
        let deviceName = UIDevice.current.name
        let deviceNick = Network.generateNick()
        
        Network.POST("device", data: [
            "deviceId": deviceId,
            "name": deviceName,
            "nick": deviceNick,
            "secret": Network.generateSecret()
        ], onComplete: { data in
            UserDefaults.standard.set(deviceId, forKey: "deviceId")
            UserDefaults.standard.set(deviceName, forKey: "deviceName")
            UserDefaults.standard.set(deviceNick, forKey: "deviceNick")
            
            onComplete(Device(id: deviceId, name: deviceName, nick: deviceNick))
        }, onFail: { error in
            if let errorApi = (error as NSError).userInfo["error"] as? String {
                if errorApi == "nick_existing" {
                    self.device(onComplete: onComplete, onFail: onFail)
                } else {
                    onFail(error)
                }
            } else {
                onFail(error)
            }
        })
    }
    
    static private func generateNick() -> String {
        let words = ["beds", "babies", "window", "condition", "toothbrush", "dust", "locket", "regret", "taste", "horses", "pencil", "silk", "chance", "snails", "lunchroom", "stew", "grandfather", "shape", "bird", "mother", "suit", "crib", "salt", "trousers", "grain", "comparison", "work", "flowers", "art", "vase", "son", "approval", "effect", "cabbage", "pipe", "root", "step", "table", "temper", "cars", "cushion", "rings", "steel", "wine", "pets", "division", "sink", "toothpaste", "harmony", "stove", "flower", "weight", "theory", "spiders", "books", "chalk", "title", "carriage", "children", "sticks", "bottle", "crayon", "stamp", "wing", "ink", "existence", "thunder", "drink", "laugh", "plants", "book", "loaf", "metal", "potato", "man", "thread", "crow", "bikes", "match", "secretary", "transport", "harbor", "scarecrow", "agreement", "letters", "bell", "education", "sand", "hot", "pocket", "earthquake", "tooth", "cherry", "judge", "army", "wren", "brick", "camp", "library", "picture", "zebra", "horse", "song", "laborer", "sky", "error", "apparel", "grip", "cake", "coach", "daughter", "collar", "grandmother", "back", "page", "servant", "pizzas", "snail", "sidewalk", "rail", "tray", "popcorn", "coal", "walk", "roll", "teeth", "train", "credit", "sign", "home", "plate", "request", "brother", "icicle", "drop", "religion", "feeling", "representative", "cover", "support", "button", "story", "competition", "sweater", "throat", "cup", "curtain", "rhythm", "pigs", "creator", "eye", "shame", "fly", "fact", "cherries", "guide", "bomb", "turn", "end", "lip", "zephyr", "produce", "bridge", "glove", "pear", "side", "country", "haircut", "bear", "brake", "language", "punishment", "cable", "silver", "vessel", "science", "health", "chin", "breath", "instrument", "record", "decision", "motion", "hole", "tree", "number", "rule", "kick", "nest", "pen", "water", "show", "ground", "earth", "observation", "leather", "door", "friend", "activity", "waste", "creature", "boundary", "fireman", "morning", "authority", "flesh", "food", "level", "veil", "spoon", "cracker", "coat", "partner", "wire", "scene", "edge", "cap", "monkey", "dinner", "jam", "dog", "sleet", "skirt", "volcano", "growth", "direction", "square", "ants", "respect", "card", "wrist", "knot", "power", "trees", "bed", "cough", "war", "plastic", "thought", "border", "stick", "rub", "rat", "experience", "seed", "interest", "truck", "cattle", "tramp", "pin", "thing", "mark", "shock", "quince", "snow", "wash", "notebook", "board", "point", "canvas", "profit", "sense", "birthday", "dirt", "lamp", "bit", "memory", "boat", "peace", "soap", "floor", "frog", "jeans", "dinosaurs", "sponge", "desire", "mailbox", "belief", "destruction", "squirrel", "hands", "visitor", "donkey", "hate", "basketball", "oven", "anger", "color", "moon", "frogs", "sock", "coil", "balance", "head", "quarter", "behavior", "ring", "toy", "plough", "impulse", "move", "insect", "stocking", "van", "tomatoes", "quill", "zinc", "income", "jewel", "afternoon", "jail", "quilt", "corn", "digestion", "pig", "cat", "geese", "church", "toe", "wrench", "women", "rainstorm", "wax", "business", "toad", "burst", "lumber", "cause", "distribution", "riddle", "cow", "rate", "planes", "egg", "minute", "rabbit", "use", "property", "force", "waves", "kitty", "time", "dad", "toes", "badge", "distance", "thumb", "sisters", "lock", "acoustics", "recess", "jump", "surprise", "doll", "money", "fog", "mist", "beef", "mouth", "cast", "basket", "mass", "cemetery", "idea", "bone", "stage", "rabbits", "island", "birth", "roof", "amount", "baseball", "nose", "hair", "snake", "reading", "nut", "cows", "bucket", "class", "road", "connection", "school", "fear", "cook", "woman", "cellar", "berry", "relation", "celery", "bag", "expert", "clover", "scent", "trail", "middle", "pail", "caption", "queen", "voyage", "songs", "turkey", "cream", "chicken", "mom", "wave", "value", "plot", "sister", "fowl", "shop", "governor", "year", "bite", "sail", "measure", "top", "pump", "flock", "winter", "front", "mask", "iron", "gun", "drain", "committee", "seashore", "chickens", "kettle", "spy", "girls", "writer", "example", "lunch", "look", "stranger", "quicksand", "wish", "ducks", "rake", "verse", "heat", "blood", "fish", "engine", "knife", "things", "eggnog", "noise", "fork", "yarn", "person", "love", "shirt", "order", "size", "sort", "pollution", "snakes", "wealth", "airplane", "paper", "slope", "boy", "pie", "tendency", "station", "statement", "neck", "knee", "view", "part", "spring", "wood", "face", "building", "attack", "blow", "fairies", "reason", "elbow", "wall", "exchange", "sound", "night", "stone", "sneeze", "ticket", "branch", "vest", "reward", "sugar", "box", "channel", "grass", "sea", "pancake", "doctor", "key", "dock", "spot", "cent", "rose", "nerve", "hour", "actor", "advertisement", "duck", "plant", "giants", "driving", "town", "straw", "watch", "chess", "meat", "slave", "pet", "club", "territory", "sun", "bath", "price", "jar", "linen", "wind", "cactus", "friction", "ghost", "street", "party", "magic", "ball", "oatmeal", "copper", "tongue", "star", "company", "volleyball", "stop", "beginner", "treatment", "cub", "market", "name", "low", "debt", "grade", "sofa", "adjustment", "oranges", "needle", "calendar", "crowd", "crook", "sheep", "achiever", "airport", "juice", "furniture", "glass", "change", "invention", "car", "eyes", "stitch", "need", "cloth", "trade", "scale", "clam", "friends", "mine", "payment", "arch", "reaction", "seat", "circle", "fang", "camera", "mountain", "addition", "cheese", "legs", "action", "rest", "group", "store", "flame", "crate", "aftermath", "carpenter", "uncle", "vegetable", "kittens", "act", "pull", "curve", "drawer", "bead", "way", "quartz", "question", "meeting", "trucks", "pleasure", "jelly", "soup", "fold", "giraffe", "twig", "hill", "foot", "ice", "string", "team", "base", "shake", "purpose", "fuel", "account", "tank", "society", "railway", "tail", "guitar", "stream", "range", "smash", "yard", "leg", "bulb", "weather", "mice", "orange", "bubble", "sack", "rain", "spade", "milk", "downtown", "line", "yak", "bells", "air", "discussion", "mint", "route", "shoes", "trains", "umbrella", "bike", "stem", "ocean", "powder", "quiet", "afterthought", "battle", "cakes", "industry", "muscle", "pickle", "letter", "week", "pot", "dress", "system", "lake", "protest", "tin", "degree", "amusement", "passenger", "talk", "gold", "cave", "playground", "slip", "history", "pan", "tub", "mitten", "zipper", "finger", "twist", "shoe", "animal", "ladybug", "dime", "discovery", "teaching", "touch", "cobweb", "field", "bat", "lace", "selection", "calculator", "pest", "month", "offer", "skin", "gate", "word", "soda", "liquid", "flavor", "spark", "position", "test", "eggs", "houses", "ear", "summer", "stomach", "poison", "insurance", "scarf", "pies", "advice", "sleep", "suggestion", "grape", "unit", "jellyfish", "humor", "throne", "smoke", "loss", "yam", "flight", "lettuce", "aunt", "structure", "thrill", "swing", "yoke", "apparatus", "nation", "finger", "sheet", "attraction", "substance", "land", "shelf", "honey", "hat", "play", "zoo", "current", "can", "expansion", "swim", "bedroom", "hook", "men", "prose", "plane", "butter", "toys", "government", "bee", "frame", "care", "horn", "hall", "rice", "ray", "minister", "basin", "development", "worm", "underwear", "dogs", "trouble", "cannon", "deer", "dolls", "form", "crack", "office", "birds", "river", "texture", "machine", "appliance", "vacation", "design", "parcel", "cats", "north", "smile", "detail", "maid", "porter", "self", "wool", "farm", "hope", "trick", "start", "robin", "boot", "day", "oil", "hammer", "push", "meal", "hobbies", "stretch", "rock", "event", "increase", "control", "place", "hose", "fire", "baby", "receipt", "hydrant", "scissors", "whistle", "death", "tent", "trip", "desk", "kiss", "rifle", "girl", "bushes", "argument", "plantation", "space", "wound", "arithmetic", "house", "crown", "bait", "tax", "flag", "ship", "holiday", "blade", "marble", "fruit", "wheel", "run", "mind", "vein", "rod", "note", "join", "writing", "coast", "limit", "tiger", "smell", "screw", "hospital", "knowledge", "fall", "steam", "whip", "alarm", "hand", "room", "arm", "voice", "quiver", "cart", "wilderness", "shade", "brass", "angle", "crime", "skate", "believe"]
        
        return "\(words[Int.random(in: 0..<876)].capitalized)\(words[Int.random(in: 0..<876)].capitalized)\(Int.random(in: 1..<999))"
    }
    
}
