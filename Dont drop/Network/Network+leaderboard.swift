//
//  Network+leaderboard.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 02/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

extension Network {
    
    public func getLeaderboard(onComplete: @escaping (Any?) -> (), onFail: @escaping (Error) -> ()) {
        device(onComplete: { device in
            Network.GET("leaderboard?deviceId=\(device.id)", onComplete: onComplete, onFail: onFail)
        }, onFail: onFail)
    }
    
    public func sendLeaderboardPoints(points: Int, onComplete: @escaping (Any?) -> (), onFail: @escaping (Error) -> ()) {
        device(onComplete: { device in
            Network.POST("leaderboard", data: [
                "points": points,
                "deviceId": device.id,
                "secret": Network.generateSecret()
            ], onComplete: onComplete, onFail: onFail)
        }, onFail: onFail)
    }
    
}
