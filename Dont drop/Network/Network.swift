//
//  Network.swift
//  Dont drop
//
//  Created by Aleksei Pugachev on 02/07/2019.
//  Copyright © 2019 Aleksei Pugachev. All rights reserved.
//

import Foundation

class Network {
    
    static private let baseProtocol = "https"
    static private let baseDomain = "drop.lenyapugachev.com"
    
    static public let baseURL = "\(baseProtocol)://\(baseDomain)"
    static public let apiURL = "\(baseProtocol)://\(baseDomain)/api/v1/"
    
    static let shared = Network()
    static let session = URLSession.shared
    
    static func GET(_ method: String, onComplete: @escaping (Any?) -> (), onFail: @escaping (Error) -> ()) {
        if let url = URL(string: "\(apiURL)\(method)") {
            session.dataTask(with: url, completionHandler: { data, response, error in
                if error == nil {
                    if let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String: Any] {
                        if let error = json["error"] as? String {
                            onFail(NSError(domain: "ApiError", code: -1, userInfo: ["error": error]))
                        } else {
                            onComplete(json)
                        }
                    } else {
                        onFail(NSError(domain: "InvalidJSON", code: -1, userInfo: nil))
                    }
                } else {
                    onFail(error!)
                }
            }).resume()
        }
    }
    
    static func POST(_ method: String, data: [String: Any], onComplete: @escaping (Any?) -> (), onFail: @escaping (Error) -> ()) {
        if let url = URL(string: "\(apiURL)\(method)") {
            var request = URLRequest(url: url)
            
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let data = try! JSONSerialization.data(withJSONObject: data, options: [])
            
            session.uploadTask(with: request, from: data) { data, response, error in
                if error == nil {
                    if let json = (try? JSONSerialization.jsonObject(with: data!, options: [])) as? [String: Any] {
                        if let error = json["error"] as? String {
                            onFail(NSError(domain: "ApiError", code: -1, userInfo: ["error": error]))
                        } else {
                            onComplete(json)
                        }
                    } else {
                        onFail(NSError(domain: "InvalidJSON", code: -1, userInfo: nil))
                    }
                } else {
                    onFail(error!)
                }
            }.resume()
        }
    }
    
}
